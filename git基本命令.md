# **git和linux基本命令**
- 删除某一行 dd  
- 删除一个字符 dw
- 插入 i  
- 保存 :w  
- 保存并退出 :wq  
- 强制退出 q！  
- 撤回 u   
- 回复上一步被撤销的操作 Ctr+r  
- 创建文件 touch  
- 创建文件夹 mkdir  
- 当前路径 pwd  
- 查看内容 cat 文件名



1. git init **把这个目录变成Git可以管理的仓库**  
2. ls -ah  **查看是否有.git文件**  
3. git add 文件名 **把文件添加到git仓库(暂存区)**
4. git commit **提交代码到仓库**  
5. git commit -m **-m后面输入本次提交说明**  
6. git status **查看仓库当前的状态**
7. git diff 文件名  **查看改变**
8. git log  **查看日志**
9. git log --pretty=oneline  **查看日志，简洁显示在一行**  
10. git reset --hard HEAD^（版本号）  **回到哪个版本，^上个，^^上上个...**
11. git reflog **查看回退之前的日志**
12. git reset --hard (版本号) **重新回来**
13. - 第一次修改 -> git add -> 第二次修改 -> git commit **只提交啊了第一次修改** 
    - 第一次修改 -> git add -> 第二次修改 -> git add -> git commit **把第二次修改提交了**  
14. git checkout -- file  **文件在工作区的修改全部撤销**
15. rm 文件名  **删除文件**
    - 删除后：从版本库中删除：  git rm 文件名
    - 删错了：git checkout 版本库里的版本替换工作区的版本，无论工作区是修改还是删除，都可以“一键还原”
16. ssh-keygen -t rsa -C "youremail@example.com" **创建SSH Key**
	- 解释：***用户主目录里找到.ssh目录，里面有id_rsa和id_rsa.pub两个文件，这两个就是SSH Key的秘钥对，id_rsa是私钥，不能泄露出去，id_rsa.pub是公钥，可以放心地告诉任何人***
17. git remote add origin git@github.com:michaelliao/learngit.git ***添加远程仓库地址 @……是github/gitlab地址***
18. git push -u origin master ***把本地库的所有内容推送到远程库上***
19. git push origin master **第一次用 -u 之后用简单命令推送就可以**
20. ## **git分支**：
   #### ***创建dev分支，然后切换到dev分支***:
   - git checkout -b dev  **创建dev分支**
   - **-b:切换分支，创建后自动切换**  
   ***加上-b相当于以下两条命令***:
   - git branch dev
   - git checkout dev
   #### ***查看当前分支***:
   - git branch
   #### ***提交***:
   - git add 文件名
   - git commit -m "注释"
   #### ***dev分支工作完成，切回master分支***：
   - git checkout master
   #### ***dev分支的工作成果合并到master分支上***：
   - git merge dev
   #### ***合并完成后就可以删除dev分支了***：
   - git branch -d dev
   ### ***删除后，查看branch，就只剩下master分支了***：
   - git branch
   
   	













